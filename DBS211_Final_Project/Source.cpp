/*
SQL Programmer
Thanh Tam Nguyen
Ttnguyen110@myseneca.ca
105760185

Application Programmer
Hien Dao The Nguyen
hnguyen110@myseneca.ca
103152195

Application Tester
Mia Le
tknle1@myseneca.ca
131101198
*/

#include <occi.h>
#include <iostream>
#include <cstring>
#include <string>
#include <cctype>

using namespace std;
using namespace oracle::occi;
using oracle::occi::Environment;
using oracle::occi::Connection;

struct Employee {
	int employeeNumber;
	string lastName;
	string firstName;
	string email;
	string phone;
	string extension;
	string reportsTo;
	string jobTitle;
	string city;

	Employee() {
		employeeNumber = 0;
	}
};


// isNumber function is used to check if a user input is a number or not
// Return true if the user input is a number and false otherwise
bool isNumber(string input) {
	bool state = true;
	if (input.length() == 0) {
		state = false;
	}
	else {
		for (int counter = 0; counter < input.length(); ++counter) {
			if (!isdigit(input[counter])) {
				state = false;
				break;
			}
		}
	}
	return state;
};

// getEmployeeNumber is used to get the employee number from the keyboard
// This function has built in validation to check if the user enter a valid input or not
// It gonna return the employee number back in the form of the string
// Using STOI to convert number in string to the int format
string getEmployeeNumber()
{
	bool stop = false;
	string result;
	while (!stop)
	{
		cout << "Enter employee number: ";
		char employeeNumber[255];
		cin >> employeeNumber;
		cin.clear();
		cin.ignore(1000, '\n');
		if (isNumber(employeeNumber))
		{
			result = employeeNumber;
			stop = true;
		}
		else
		{
			cout << "Invalid employee number, please try again: " << endl;
		}
	}
	return result;
};


// printMenu is used to print out the menu header
void printMenu() {
	cout << "********************* HR Menu *********************" << endl;
	cout << "1)	Find Employee" << endl;
	cout << "2)	Employees Report" << endl;
	cout << "3)	Add Employee" << endl;
	cout << "4)	Update Employee" << endl;
	cout << "5)	Remove Employee" << endl;
	cout << "0)	Exit" << endl;
	cout << ">>> ";
}

// The menu function is going to get the user option from 1 to 5
// It also has built in validation to check the user input and prevent user to enter an invalid input
// Menu will return back to the main the user option as an integer
int menu() {
	printMenu();
	bool stop = false;
	string user_input = "";
	int user_option = 0;
	while (!stop) {
		cin >> user_input;
		cin.clear();
		cin.ignore(1000, '\n');
		if (isNumber(user_input)) {
			user_option = stoi(user_input);
			if (user_option >= 0 && user_option <= 5) {
				stop = true;
			}
			else {
				cout << "Invalid option, please choose option from 1 to 5: ";
			}
		}
		else {
			cout << "Invalid input, please enter a number: ";
		}
	}
	return user_option;
};

// Find employee function is used to search for the employee inside the database
// If it found a record, then it will assign it to the emp and return back true or 1 if it found a record
// Otherwise, it will return 0 or false
int findEmployeee(Connection* conn, int employeeNumber, struct Employee* emp) {
	if (conn != nullptr && emp != nullptr) {
		try {
			Statement* statement = conn->createStatement();
			string query = "";
			query += "SELECT *";
			query += " FROM(";
			query += " SELECT employees.employeenumber,";
			query += " employees.lastname,";
			query += " employees.firstname,";
			query += " employees.email,";
			query += " offices.phone,";
			query += " employees.extension,";
			query += " managers.firstname || ' ' || managers.lastname, ";
			query += " employees.jobtitle,";
			query += " offices.city";
			query += " FROM dbs211_employees employees";
			query += " LEFT OUTER JOIN dbs211_offices offices ON employees.officecode = offices.officecode";
			query += " LEFT OUTER JOIN dbs211_employees managers";
			query += " ON employees.reportsto = managers.employeenumber";
			query += " )";
			query += " WHERE employeenumber = ";
			query += to_string(employeeNumber);
			ResultSet* resultset = statement->executeQuery(query);

			if (resultset->next()) { 
				emp->employeeNumber = resultset->getInt(1);
				emp->lastName = resultset->getString(2);
				emp->firstName = resultset->getString(3);
				emp->email = resultset->getString(4);
				emp->phone = resultset->getString(5);
				emp->extension = resultset->getString(6);
				emp->reportsTo = resultset->getString(7);
				emp->jobTitle = resultset->getString(8);
				emp->city = resultset->getString(9);

				delete[] resultset;
				resultset = nullptr;
				conn->terminateStatement(statement);

				return 1;
			}
			else {
				return 0;
			}
		}
		catch (SQLException& err) {
			cout << err.getErrorCode() << ": " << err.getMessage();
			return 0;
		}
	}
	else {
		cout << "Invaid parameters" << endl;
		return 0;
	}
};

// Display employee will get the employee number and search for the data inside the database
// Then it gonna display employee information if it find anything back from the database
void displayEmployee(Connection* conn, struct Employee* emp) {
	if (conn != nullptr) {
		try {
			int employeeNumber = stoi(getEmployeeNumber());
			if (findEmployeee(conn, employeeNumber, emp)) {
				cout << "employeeNumber = " << emp->employeeNumber << endl;
				cout << "lastName = " << emp->lastName << endl;
				cout << "firstName = " << emp->firstName << endl;
				cout << "email = " << emp->email << endl;
				cout << "phone = " << emp->phone << endl;
				cout << "extension = " << emp->extension << endl;
				cout << "reportsTo = " << emp->reportsTo << endl;
				cout << "jobTitle = " << emp->jobTitle << endl;
				cout << "city = " << emp->city << endl;
			}
			else {
				cout << "Employee " << employeeNumber << " does not exist." << endl;
			}
		}
		catch (SQLException& err) {
			cout << err.getErrorCode() << ": " << err.getMessage();
		}
	}
	else {
		cout << "Invaid parameters" << endl;
	}
};

// printHeader gonna create the header for results it gets back from the database
void printHeader() {
	cout.width(8);
	cout << "E";
	cout.width(20);
	cout << "Employee Name";
	cout.width(35);
	cout << "Email";
	cout.width(20);
	cout << "Phone";
	cout.width(10);
	cout << "Ext";
	cout.width(25);
	cout << "Manager" << endl;
}

// displayAllEmployees is going to make a query to the database to search for all employee records
// It gonna join with the offices and employees table to get office location and the name of the manager for each employee
// Finally display all employee information out to the screen in a format
void displayAllEmployees(Connection* conn) {
	try {
		Statement* statement = conn->createStatement();
		string query = "";
		query += "SELECT *";
		query += " FROM(";
		query += " SELECT employees.employeenumber,";
		query += " employees.lastname,";
		query += " employees.firstname,";
		query += " employees.email,";
		query += " offices.phone,";
		query += " employees.extension,";
		query += " managers.firstname || ' ' || managers.lastname, ";
		query += " employees.jobtitle,";
		query += " offices.city";
		query += " FROM dbs211_employees employees";
		query += " LEFT OUTER JOIN dbs211_offices offices ON employees.officecode = offices.officecode";
		query += " LEFT OUTER JOIN  dbs211_employees managers";
		query += " ON employees.reportsto = managers.employeenumber";
		query += " ) ORDER BY employeenumber";
		ResultSet* resultset = statement->executeQuery(query);

		bool empty = true;
		while (resultset->next()) {
			if (empty) {
				printHeader();
				empty = false;
			}
			string employee_fullname = resultset->getString(2) + " " + resultset->getString(3);
			string manager_fullname = resultset->getString(7);
			cout.width(8);
			cout << resultset->getInt(1);
			cout.width(20);
			cout << employee_fullname;
			cout.width(35);
			cout << resultset->getString(4);
			cout.width(20);
			cout << resultset->getString(5);
			cout.width(10);
			cout << resultset->getString(6);
			cout.width(25);
			cout << manager_fullname << endl;
		}
		if (empty) {
			cout << "There is no employees� information to be displayed." << endl;
		}
		delete[] resultset;
		resultset = nullptr;
		conn->terminateStatement(statement);
	}
	catch (SQLException& err) {
		cout << err.getErrorCode() << ": " << err.getMessage();
	}
}

// Insert employee will insert an employee record to the database
// This function gonna search for any record in the database to see if the current employee exist or not
// If not then it will insert the employee information to the database and make a commit
// Otherwise, the operation will be ignore
void insertEmployee(Connection* conn, struct Employee* emp) {
	if (conn != nullptr) {
		try {
			if (!findEmployeee(conn, emp->employeeNumber, emp)) {
				Statement* statement = conn->createStatement();
				string query = "";
				query += "INSERT INTO dbs211_employees(employeenumber, lastname, firstname, extension, email, officecode, reportsto, jobtitle)";
				query += " VALUES (";
				query += " " + to_string(emp->employeeNumber);
				query += ", '" + emp->lastName + "'";
				query += ", '" + emp->firstName + "'";
				query += ", '" + emp->extension + "'";
				query += ", '" + emp->email + "'";
				query += ", '1'";
				query += ", 1002";
				query += ", '" + emp->jobTitle + "'";
				query += ")";
				statement->executeUpdate(query);
				conn->commit();
				conn->terminateStatement(statement);
				cout << "The new employee is added successfully." << endl;
			}
			else {
				cout << "An employee with the same employee number exists." << endl;
			}
		}
		catch (SQLException& err) {
			cout << err.getErrorCode() << ": " << err.getMessage();
		}
	}
	else {
		cout << "Invaid parameters" << endl;
	}
}

// UpdateEmployee will go into the database and search for the employee need to be updated
// If the update function finds a record, then it gonna take the new phone extension
// And update the new phone extension to the database based on the matching user ID
// If the function found nothing, it will cancel the operation
void updateEmployee(Connection* conn, int employeeNumber) {
	if (conn != nullptr) {
		try {
			Employee* emp = new Employee();
			if (findEmployeee(conn, employeeNumber, emp)) {
				cout << "Enter the new employee extension: ";
				cin >> emp->extension;
				cin.clear();
				cin.ignore(1000, '\n');
				Statement* statement = conn->createStatement();
				string query = "";
				query += "UPDATE dbs211_employees";
				query += " SET extension =";
				query += " '" + emp->extension + "'";
				query += " WHERE employeenumber =";
				query += " " + to_string(emp->employeeNumber);
				statement->executeUpdate(query);
				conn->commit();
				conn->terminateStatement(statement);
				delete emp;
				emp = nullptr;
				cout << "The employee is updated." << endl;
			}
			else {
				cout << "The employee does not exist" << endl;
			}
		}
		catch (SQLException& err) {
			cout << err.getErrorCode() << ": " << err.getMessage();
		}
	}
	else {
		cout << "Invaid parameters" << endl;
	}
}

// DeleteEmployee will take the employee number and search for that employee 
// If the employee exists, then it will run the sql command to delete the user from the database
// Otherwise, the operation will be cancel and no record will be removed
void deleteEmployee(Connection* conn, int employeeNumber) {
	if (conn != nullptr) {
		try {
			Employee* emp = new Employee();
			if (findEmployeee(conn, employeeNumber, emp)) {
				Statement* statement = conn->createStatement();
				string query = "";
				query += "DELETE FROM dbs211_employees WHERE employeenumber = ";
				query += to_string(emp->employeeNumber);
				statement->executeUpdate(query);
				conn->commit();
				conn->terminateStatement(statement);
				delete emp;
				emp = nullptr;
				cout << "The employee is deleted." << endl;
			}
			else {
				cout << "The employee does not exist" << endl;
			}
			
		}
		catch (SQLException& err) {
			cout << err.getErrorCode() << ": " << err.getMessage();
		}
	}
	else {
		cout << "Invaid parameters" << endl;
	}
}

int main() {
	try {
		Environment* env = nullptr;
		Connection* conn = nullptr;

		string username = "dbs211_202b18";
		string password = "19060254";
		string server = "myoracle12c.senecacollege.ca:1521/oracle12c";

		env = Environment::createEnvironment(Environment::DEFAULT);
		conn = env->createConnection(username, password, server);

		Employee* emp = new Employee();
		int option = 0;
		while ((option = menu()) != 0) {
			if (option == 1) {
				displayEmployee(conn, emp);
			}
			else if (option == 2) {
				displayAllEmployees(conn);
			}
			else if (option == 3) {
				cout << "Employee Number: ";
				cin >> emp->employeeNumber;
				cin.clear();
				cin.ignore(1000, '\n');
				cout << "Last Name: ";
				getline(cin, emp->lastName, '\n');
				cout << "First Name: ";
				getline(cin, emp->firstName, '\n');
				cout << "Email: ";
				getline(cin, emp->email, '\n');
				cout << "Extension: ";
				getline(cin, emp->extension, '\n');
				cout << "Job Title: ";
				getline(cin, emp->jobTitle, '\n');
				cout << "City: ";
				getline(cin, emp->city, '\n');
				insertEmployee(conn, emp);
			}
			else if (option == 4) {
				int empNumber = stoi(getEmployeeNumber());
				updateEmployee(conn, empNumber);
			}
			else if (option == 5) {
				int empNumber = stoi(getEmployeeNumber());
				deleteEmployee(conn, empNumber);
			}
		}

		delete emp;
		emp = nullptr;

		env->terminateConnection(conn);
		Environment::terminateEnvironment(env);
	}
	catch (SQLException& err) {
		cout << err.getErrorCode() << ": " << err.getMessage();
	}
	return 0;
} 